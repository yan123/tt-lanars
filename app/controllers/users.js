var path            = require('path');
var mongoose        = require('mongoose');
var crypto          = require('crypto');
var dbSchemas       = require(path.join(__dirname, '../../app/models'));
var dbController     = require(path.join(__dirname, '../../app/libs/dbController'));

function SearchUsers(req, res){
    dbController.createDBConnection(req.headers.authorization, res, function(db, res){
        var User = db.model("Users", dbSchemas.UserSchema);
        var requestBody = req.query;
        var updateValuesMask = [
            'name',
            'email'
        ];
        var updateValues = {};
        for (var key in requestBody) {
            if (updateValuesMask.indexOf(key) != -1) {
                updateValues[key] = requestBody[key];
            }
        };
        User.find(updateValues, {
            "_id":          1,
            "name":         1,
            "email":        1,
            "phone":        1
        }, function(err, users) {
            if (err) {
                dbController.closeDBConnection(db, res, 500, {
                    success:    false,
                    response:   false,
                    message:    'This login already exists [' + err.message + ']'
                });
            }
            if (users){
                dbController.closeDBConnection(db, res, 200, {
                    success: true,
                    response: users,
                    message: false
                });
            } else {
                dbController.closeDBConnection(db, res, 500, {
                    success:    false,
                    response:   false,
                    message:    'Error in getting Users list'
                });
            }
        });
    });
}
function getUserById(req, res){
  dbController.createDBConnection(req.headers.authorization, res, function(db, res){
      var UserId = req.params.id;
      var User = db.model("Users", dbSchemas.UserSchema);
      User.findOne({ "_id": UserId }, {
        "_id":          1,
        "name":         1,
        "email":        1,
        "phone":        1
      }, function(err, user) {
          if (err) {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'This login already exists [' + err.message + ']'
              });
          }
          if (user){
            dbController.closeDBConnection(db, res, 200, {
                success: true,
                response: user,
                message: false
            });
          } else {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'Error in getting Users list'
              });
          }
        });
    });
}
module.exports.getUserById  = getUserById;
module.exports.SearchUsers  = SearchUsers;
