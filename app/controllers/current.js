var path            = require('path');
var mongoose        = require('mongoose');
var crypto          = require('crypto');
var dbSchemas       = require(path.join(__dirname, '../../app/models'));
var dbController     = require(path.join(__dirname, '../../app/libs/dbController'));

function getCurrentUser(req, res){
    dbController.createDBConnection(req.headers.authorization, res, function(db, res){
        var User = db.model("Users", dbSchemas.UserSchema);
        User.findOne({ api_token: req.headers.authorization }, function(err, users) {
            if (err) {
                dbController.closeDBConnection(db, res, 500, {
                    success:    false,
                    response:   false,
                    message:    'This login already exists [' + err.message + ']'
                });
            }
            if (users){
                dbController.closeDBConnection(db, res, 200, {
                    success: true,
                    response: users,
                    message: false
                });
            } else {
                dbController.closeDBConnection(db, res, 500, {
                    success:    false,
                    response:   false,
                    message:    'Error in getting Users list'
                });
            }
        });
    });
}


function updateUser(req, res){
    var updateId = req.params.id; //ID user
    var requestBody = req.body; //PUT params

    //Update permissions
    var updateValuesMask = [
        'name',
        'surname',
        'role',
        'password'
    ];

    var updateValues = {}; //PUT params with permissions

    for (var key in requestBody) {
        if (updateValuesMask.indexOf(key) != -1) {
            updateValues[key] = requestBody[key];
        }
    }

    updateValues.updated_at = Date.now(); //Insert update time

    //Encode user password
    if (updateValues.password) {
        updateValues.password = crypto.createHash('md5').update(requestBody.password).digest('hex');
    }

    dbController.createDBConnection(req.headers.authorization, res, function(db, res){
        var User = db.model("Users", dbSchemas.UserSchema);

        User.findOneAndUpdate({ _id: updateId }, updateValues, {upsert:true}, function(err, doc){
            if (err) {

                dbController.closeDBConnection(db, res, 500, {
                    success: false,
                    response: false,
                    message: 'Update error. DB message: '+err.message
                });
            }
            if (doc) {
              console.log(doc['name']);
              console.log('Test');
                dbController.closeDBConnection(db, res, 200, {
                    success: true,
                    response: {
                        "user": {
                            "id":           doc['_id'],
                            "login":        doc['login'],
                            "updated_at":   requestBody.updated_at || doc['updated_at'],
                            "created_at":   doc['created_at'],
                            "role":         requestBody.role || doc['role'],
                            "surname":      requestBody.surname || doc['surname'],
                            "name":         requestBody.name || doc['name']
                        }
                    },
                    message: false
                });

            } else {

                dbController.closeDBConnection(db, res, 500, {
                    success: false,
                    response: false,
                    message: 'Update error'
                });

            }
        });
    });
}
module.exports.updateUser  = updateUser;
module.exports.getCurrentUser  = getCurrentUser;
