var path = require('path');

module.exports.admin = function(req, res){
    res.sendFile(path.join(__dirname, '../../build', 'admin.html'));
};

module.exports.current = require('./current');
module.exports.auth = require('./auth');
module.exports.register = require('./register')
module.exports.users = require('./users');
module.exports.items = require('./items')
module.exports.image = require('./image.item');
