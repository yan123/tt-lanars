var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    email:      {type: String,  required:   true},
    name:       {type: String,  default:    null},
    phone:      {type: String, default:    null},
    api_token:  {type: String,  default:    null},
    password:   {type: String,  required:   true},
    created_at: {type: Date,    default:    Date.now},
    updated_at: {type: Date,    default:    Date.now}
});


var ItemsSchema =  new mongoose.Schema({
    image:        {type: String,  default:    null},
    title:        {type: String,  required:    true},
    description:  {type: String,  required:    true},
    user_id:      {type: String,  default:   null},
    user:         {type: Object,  default:   null},
    created_at:   {type: Date,    default:    Date.now},
    updated_at:   {type: Date,    default:    Date.now}
});

module.exports.UserSchema         = UserSchema;
module.exports.ItemsSchema        = ItemsSchema;
