module.exports = function(res, status, success, response, message){
    res.status(status).send(JSON.stringify({
        success: success,
        response: response,
        message: message
    }));
};