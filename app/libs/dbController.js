var path        = require('path');
var mongoose    = require('mongoose');
mongoose.Promise = require('bluebird');
var config      = require(path.join(__dirname, '../../app/libs/config'));
var dbSchemas   = require(path.join(__dirname, '../../app/models'));

function createDBConnection(token, res, callback){

      var mongodb = config.get('mongoose:uri');
      var db = mongoose.connect(mongodb,{
        useMongoClient: true
      });
      if (token != false) {
        db.once('open', function() {
            var User = db.model("Users", dbSchemas.UserSchema);

            User.where({ api_token: token }).findOne(function (err, user) {
                if (err) {
                    closeDBConnection(db, res, 500, {
                        status:     false,
                        response:   false,
                        message:    'db connection error: ' + err.message
                    });
                }

                if (user) {
                    callback(db, res);
                } else {
                    closeDBConnection(db, res, 403, {
                        status:     false,
                        response:   false,
                        message:    'Invalid Api_token'
                    });
                }
            });
        });

        db.on('error', function (err) {
            console.log('db connection error: ' + err.message);
        });

    } else callback(db, res);
}

function closeDBConnection(db, res, status, data){
    if (db._hasOpened) db.close();

    res.status(status).send(JSON.stringify({
        success:    data.success,
        response:   data.response,
        message:    data.message
    }));

    return null;
}

module.exports.createDBConnection = createDBConnection;
module.exports.closeDBConnection = closeDBConnection;
