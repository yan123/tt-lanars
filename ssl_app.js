var express     = require('express');
var routes      = require('./app/routes');
var controllers = require('./app/controllers');
var config      = require('./app/libs/config');

require('events').EventEmitter.defaultMaxListeners = Infinity;

var app = express();

routes.setup(app, controllers);

app.use(express.static( __dirname + '/build' ));



//********* HTTPS ********************************************************
var fs          = require('fs');
var http        = require('http');
var https       = require('https');

var options = {
    key: fs.readFileSync(config.get('ssl:key')),
    cert: fs.readFileSync(config.get('ssl:crf')),
    requestCert: true
};

var server = https.createServer(options, app);
server.listen(config.get('port_https'), config.get('ip'), function() {
    console.log("Express server listening on port " + config.get('port'));
});



//******** HTTP redirect to HTTPS *****************************************
http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
}).listen(config.get('port'), config.get('ip'));