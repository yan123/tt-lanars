mainApp.factory('Api', [
    '$http',
    '$state',
    'authControl',
    'Toast',
    function($http, $state, authControl, Toast){
        var baseUrl = 'api/';

        function onError(error){
            if (error.data.message == 'Invalid Api_token') {
                authLogout()
            } else {
                Toast.error('Status: ' + error.status + '. Error: ' + error.data.message);
            }
        }
        function authCheck(){
            return (authControl.getToken() != false);
        }
        function onAuthFalse(){}

        function authLogin(params) {
            $http.post(baseUrl + 'login', params)
                .then(function (response) {
                    if (response.data.success == true) {
                        window.localStorage.setItem('auth_info', JSON.stringify(response.data.response));
                        authControl.setAuthStatus(true, response.data.response.api_token);
                        return true;
                    } else {
                        Toast.error('Authorization error');
                        authControl.setAuthStatus(false, false);
                        return false;
                    }
                }, onError);
        }
        function authRegister(params){
          $http.post(baseUrl + 'register', params)
              .then(function (response) {
                  if (response.data.success == true) {
                      window.localStorage.setItem('auth_info', JSON.stringify(response.data.response));
                      authControl.setAuthStatus(true, response.data.response.api_token);
                      return true;
                  } else {
                      Toast.error('Register error');
                      authControl.setAuthStatus(false, false);
                      return false;
                  }
              }, onError);
        }

        function getCurrentUser(callback){
          $http({
            url: baseUrl + 'me',
            method: "GET",
            headers: {
              Authorization : authControl.getToken()
            }
          }).then(function (response) {
                callback(response.data);
            }, onError);
        }
        function authLogout() {
            window.localStorage.removeItem('auth_info');
            authControl.setAuthStatus(false, false);
            $state.go('login');
            return true;
        }
        function getUsers(data, callback){
            $http({
              method: "GET",
              url : baseUrl + 'user?' + (!!data.name ?  'name='+data.name+'&' : '') + (!!data.email ? 'email='+data.email : ''),
              headers: {
                Authorization : authControl.getToken()
              }
            }).then(function (response) {
                    callback(response.data);
                }, onError);
        }

        function searchItems(data, callback){
          $http({
            method: "GET",
            url : baseUrl + 'item?' + (!!data.title ?  'title='+data.title+'&' : '') + (!!data.user_id ? 'user_id='+data.user_id+'&' : '')  + (!!data.order_by ? 'order_by='+data.order_by+'&' : '')  + (!!data.order_type ? 'order_type='+data.order_type+'&' : ''),
            headers: {
              Authorization : authControl.getToken()
            }
          }).then(function (response) {
                  callback(response.data);
              }, onError);
        }

        function updateUser(id, params, callback) {
            $http({
              method: "PUT",
              url: baseUrl + 'me/' + id,
              data: params,
              headers: {
                Authorization : authControl.getToken()
              }
            })
                .then(function (response) {
                    callback(response.data);
                }, onError);
        }
        function getSingleItem(id, callback){
          $http({
            method: "GET",
            url: baseUrl + 'item/' + id,
            headers: {
              Authorization : authControl.getToken()
            }
          })
              .then(function (response) {
                  callback(response.data);
              }, onError);
        }

        function deleteMyItem(id, callback){
          $http({
            method: "DELETE",
            url : baseUrl + 'item/'+id,
            headers: {
              Authorization : authControl.getToken()
            }
          }).then(function (response) {
                  callback(response.data);
          }, onError);
        }

        function getMyItems(callback){
            $http({
              method: "GET",
              url : baseUrl + 'item',
              headers: {
                Authorization : authControl.getToken()
              }
            })
            .then(function (response) {
                    callback(response.data);
            }, onError);
        }

        function editMyItem(id, params, callback){
          $http({
            method: "PUT",
            url: baseUrl + 'item/' + id,
            data: params,
            headers: {
              Authorization : authControl.getToken()
            }
          })
              .then(function (response) {
                  callback(response.data);
              }, onError);
        }

        function deleteMyItemImg(id, callback){
          $http({
            method: "DELETE",
            url : baseUrl + 'item/'+id+'/image',
            headers: {
              Authorization : authControl.getToken()
            }
          }).then(function (response) {
                  callback(response.data);
          }, onError);
        }

        function getSingleUser(id, callback) {
          $http({
            method: "GET",
            url : baseUrl + 'user/'+id,
            headers: {
              Authorization : authControl.getToken()
            }
          }).then(function (response) {
                  callback(response.data);
          }, onError);
        }

        function addMyItem(params, callback){
            $http({
              method: "PUT",
              url : baseUrl + 'item',
              headers: {
                Authorization : authControl.getToken()
              },
              data: params
            })
            .then(function (response) {
                    callback(response.data);
            }, onError);
        }

        return authCheck()?{
            Auth: {
                login:  authLogin,
                register: authRegister,
                logout: authLogout
            },
            Users: {
                search:    getUsers,
                single:     getSingleUser
            },
            Current: {
              get: getCurrentUser,
              update: updateUser
            },
            Items: {
              get: getMyItems,
              search: searchItems ,
              single: getSingleItem,
              add: addMyItem,
              delete: deleteMyItem,
              edit: editMyItem,
              deleteImg: deleteMyItemImg
            }
        }:{
            Auth: {
                login:  authLogin,
                register: authRegister,
                logout: authLogout
            },
            Users: {
                search:    onAuthFalse,
                single:     onAuthFalse
            },
            Current: {
              get: onAuthFalse,
              update: onAuthFalse
            },
            Items: {
              get: onAuthFalse,
              search: onAuthFalse ,
              single: onAuthFalse,
              add: onAuthFalse,
                delete: onAuthFalse,
                edit: onAuthFalse,
                deleteImg: onAuthFalse
            }
        };
    }
]);
