mainApp.service('Toast', [
    '$mdToast',
    function($mdToast) {
        this.message = function (message){
            $mdToast.show({
                hideDelay: 3000,
                position: 'bottom right',
                template: '<md-toast><span class="md-toast-text" data-flex>'+ message +'</span></md-toast>'
            });
        };

        this.error = function(error) {
            $mdToast.show({
                hideDelay: 3000,
                position: 'bottom right',
                template: '<md-toast class="error"><span class="md-toast-text" data-flex>'+ error +'</span></md-toast>'
            });
        }
    }
]);