mainApp.service('authControl',[
    '$state',
    function($state) {
        this.authStatus = false;
        this.token = null;
        if (window.localStorage.getItem('auth_info') != undefined) {
            this.authStatus = true;
            this.token = JSON.parse(window.localStorage.getItem('auth_info')).api_token;
        }

        this.setAuthStatus = function(status, token){
            this.authStatus = status;
            this.token = token;
            if (status == true) $state.go('main.users');
        };

        this.getToken = function(){
            return this.authStatus?this.token:false;
        };
    }
]);