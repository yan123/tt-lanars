mainApp.config([
    '$translateProvider',
    function ($translateProvider) {
        $translateProvider.useSanitizeValueStrategy('escapeParameters');

        $translateProvider.useStaticFilesLoader({
            prefix: 'locals/admin-',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage(window.localStorage.getItem('lang') || 'ru');
    }]);
 
