mainApp.controller('settingsDialogCtrl', [
    '$scope',
    '$mdDialog',
    '$translate',
    function($scope, $mdDialog, $translate){
        $scope.language = $translate.use();

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.changeLng = function( lang ) {
            $translate.use( lang );
            window.localStorage.setItem('lang', lang);
            $mdDialog.hide();
        };

    }
]);