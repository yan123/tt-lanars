mainApp.controller('confirmDialogCtrl', [
    '$scope',
    '$mdDialog',
    'theScope',
    '$translate',
    function($scope, $mdDialog, theScope, $translate){
        $translate([
            'confirmDialog.title',
            'confirmDialog.close',
            'confirmDialog.cancel'
        ]).then(function (data) {
            $scope.fieldValues = data;
        });

        $scope.data = theScope;

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.sendOk = function(){
            $mdDialog.hide();
        };
    }
]);