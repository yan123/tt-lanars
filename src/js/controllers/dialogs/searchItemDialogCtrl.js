mainApp.controller('searchItemDialogCtrl', [
    '$scope',
    '$mdDialog',
    '$translate',
    function($scope, $mdDialog, $translate){
        $translate([
            'searchitemDialog.titleh1',
            'searchitemDialog.title',
            'searchitemDialog.user_id',
            'searchitemDialog.done',
            'searchitemDialog.close',
            'searchitemDialog.filter.title',
            'searchitemDialog.filter.created_at',
            'searchitemDialog.filter.asc',
            'searchitemDialog.filter.desc'

        ]).then(function (data) {
            $scope.fieldValues = data;
        });
        $scope.search = {};
        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.changeLng = function() {
            $mdDialog.hide();
        };

        $scope.sendForm = function(){
            $mdDialog.hide($scope.search);
        };
    }
]);
