mainApp.controller('updateItemDialogCtrl', [
    '$scope',
    '$mdDialog',
    '$translate',
    'itemOldData',
    function($scope, $mdDialog, $translate, itemOldData){
        $translate([
            'updateItemDialog.title',
            'updateItemDialog.close',
            'updateItemDialog.description',
            'updateItemDialog.titleh1',
            'updateItemDialog.done',
        ]).then(function (data) {
            $scope.fieldValues = data;
        });
        console.log(itemOldData);
        $scope.updateItem = {
            title:       itemOldData.title,
            description: itemOldData.description
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.changeLng = function() {
            $mdDialog.hide();
        };

        $scope.sendForm = function(){
            $mdDialog.hide($scope.updateItem);
        };
    }
]);
