mainApp.controller('usersCtrl', [
    '$scope',
    '$state',
    'Api',
    'authControl',
    'Toast',
    '$rootScope',
    '$translate',
    '$mdDialog',
    function($scope, $state, Api, authControl, Toast, $rootScope, $translate, $mdDialog){

        getTranslates();
        var translateListener = $rootScope.$on('$translateChangeSuccess', getTranslates);
        $scope.$on('$destroy', translateListener);

        $scope.table = {
            Style: [100, 60, 150, 90, 90, 150, 100],
            Data: [],
            currentPage: 1,
            pageSize: 10
        };

        $scope.card = {
          Data : []
        }

        updateUsersList();

        $scope.searchUserDialog = function(event) {
            $mdDialog.show({
                controller: 'searchUserDialogCtrl',
                templateUrl: 'templates/dialogs/searchUserDialog.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose:true,
                fullscreen: false
            }).then(function(data){
                Api.Users.search(data, function(data){
                    if (data.success == true) {
                      console.log( data.response)
                      $scope.searchStatus = true;
                      $scope.card.Data = data.response;
                    };
                });
            });
        };

        $scope.updateUserDialog = function(event, user) {
            $mdDialog.show({
                locals: {
                    userOldData: user
                },
                controller: 'updateUserDialogCtrl',
                templateUrl: 'templates/dialogs/updateUserDialog.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose:true,
                fullscreen: false
            }).then(function(data){

                Api.Current.update(user['_id'], data, function(data){
                    if (data.success == true) updateUsersList();
                });

            });
        };

        $scope.deleteUserDialog = function(event, user) {
            $mdDialog.show({
                locals: {
                    theScope: {
                        message: 'Удалить пользователя ID:<br>' + user._id + ' ?',
                        buttonName: 'Удалить'
                    }
                },
                controller: 'confirmDialogCtrl',
                templateUrl: 'templates/dialogs/confirmDialog.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function(){
                Api.Users.delete(user['_id'], function(data){
                    if (data.success == true) updateUsersList();
                });
            });
        };

        $scope.filterRoles = function(role){
            var currentRole = JSON.parse(window.localStorage.getItem('auth_info')).user.role;

            var mask = {
                Root: ['Root', 'Admin', 'User'],
                Admin: ['User']
            };

            if (mask[currentRole].indexOf(role) != -1) {
                return true;
            } else {
                return false;
            }
        };


        function updateUsersList(){
            Api.Current.get(function(data){
                $scope.table.Data = data.response;
                $scope.loaded  = true;
            });
        }

        function getTranslates(){
            $translate([
                'Users.btn.search',
                'Users.table.header.title',
                'Users.table.body.head.id',
                'Users.table.body.head.email',
                'Users.table.body.head.name',
                'Users.table.body.head.created_at',
                'Users.table.body.head.updated_at',
                'Users.table.body.head.phone',
                'Users.table.body.head.actions',
                'Users.table.body.actions.edit',
                'Users.table.body.actions.delete',
                'Users.table.body.roles.Root',
                'Users.table.body.roles.Admin',
                'Users.table.body.roles.User',
                'Users.table.footer.rows',
                'Users.table.footer.of',
                'Users.actions.edit.confirmMSG',
                'Users.actions.edit.confirmBTN',
                'Users.actions.edit.success',
                'Users.actions.edit.error',
                'Users.actions.delete.confirmMSG',
                'Users.actions.delete.confirmBTN',
                'Users.actions.delete.success',
                'Users.actions.delete.error',
                //card
                'Users.search.header.title',
                'Users.search.card.btn.view'
            ]).then(function (data) {
                $scope.translations = data;
            });
        }
    }
]);
