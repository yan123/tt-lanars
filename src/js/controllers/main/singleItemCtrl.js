mainApp.controller('singleItemCtrl', [
    '$scope',
    '$state',
    'Api',
    'authControl',
    'Toast',
    '$rootScope',
    '$translate',
    '$mdDialog',
    '$stateParams',
    function($scope, $state, Api, authControl, Toast, $rootScope, $translate, $mdDialog, $stateParams){

            getTranslates();
            updateItemList();
            var translateListener = $rootScope.$on('$translateChangeSuccess', getTranslates);
            $scope.$on('$destroy', translateListener);


            $scope.deleteSingleItem = function(event){
              $mdDialog.show({
                  locals: {
                      theScope: {
                          message: 'Удалить работу ?',
                          buttonName: 'Удалить'
                      }
                  },
                  controller: 'confirmDialogCtrl',
                  templateUrl: 'templates/dialogs/confirmDialog.html',
                  parent: angular.element(document.body),
                  targetEvent: event,
                  clickOutsideToClose: true,
                  fullscreen: false
              }).then(function(){
                  Api.Items.delete($stateParams.id, function(data){
                      if (data.success == true) $state.go('main.items');
                  });
              });
            };




            $scope.editSingleItem = function(event, item) {
                  $mdDialog.show({
                      locals: {
                          itemOldData: item
                      },
                      controller: 'updateItemDialogCtrl',
                      templateUrl: 'templates/dialogs/updateItemDialog.html',
                      parent: angular.element(document.body),
                      targetEvent: event,
                      clickOutsideToClose:true,
                      fullscreen: false
                  }).then(function(data){
                    Api.Items.edit($stateParams.id, data, function(data){
                        if (data.success == true) updateItemList();
                    });
                  });
              };


            $scope.addImage = function(event, item) {
                  $mdDialog.show({
                      locals: {
                          id: item._id
                      },
                      controller: 'addImageDialogCtrl',
                      templateUrl: 'templates/dialogs/addImageDialog.html',
                      parent: angular.element(document.body),
                      targetEvent: event,
                      clickOutsideToClose:true,
                      fullscreen: false
                  }).then(function(data){
                      updateItemList()
                  });
            };

            $scope.deleteImage = function(event) {
                  $mdDialog.show({
                    locals: {
                        theScope: {
                            message: 'Удалить изображение ?',
                            buttonName: 'Удалить'
                        }
                    },
                    controller: 'confirmDialogCtrl',
                    templateUrl: 'templates/dialogs/confirmDialog.html',
                    parent: angular.element(document.body),
                    targetEvent: event,
                    clickOutsideToClose: true,
                    fullscreen: false
                  }).then(function(data){
                    Api.Items.deleteImg($stateParams.id, function(data){
                        if (data.success == true) updateItemList();
                    });
                  });
              };

              $scope.card = {
                  Data: [],
                  authControl_api : authControl.getToken()
              };

              function updateItemList(){
                Api.Items.single($stateParams.id,function(data){
                    $scope.card.Data = data.response;
                    $scope.loaded  = true;
                });
              }

              function getTranslates(){
                  $translate([
                      'Items.btn.addImg',
                      'Items.btn.deleteImg',
                      'Items.btn.editItem',
                      'Items.btn.deleteItem',
                      'Items.card.header.title',
                      'Items.card.btn.view'
                  ]).then(function (data) {
                      $scope.translations = data;
                  });
              }

          }
  ]);
