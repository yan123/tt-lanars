mainApp.controller('itemsCtrl', [
    '$scope',
    '$state',
    'Api',
    'authControl',
    'Toast',
    '$rootScope',
    '$translate',
    '$mdDialog',
    function($scope, $state, Api, authControl, Toast, $rootScope, $translate, $mdDialog){

      getTranslates();
      updateItemsList();
      var translateListener = $rootScope.$on('$translateChangeSuccess', getTranslates);
      $scope.$on('$destroy', translateListener);


      $scope.searchItemDialog = function(event) {
            $mdDialog.show({
                controller: 'searchItemDialogCtrl',
                templateUrl: 'templates/dialogs/searchItemDialog.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose:true,
                fullscreen: false
            }).then(function(data){
              Api.Items.search(data, function(data){
                  if (data.success == true) {
                    $scope.card.Data = data.response;
                    $scope.loaded  = true;
                  }
              });
            });
        };
      $scope.addItemDialog = function(event) {
            $mdDialog.show({
                controller: 'addItemDialogCtrl',
                templateUrl: 'templates/dialogs/addItemDialog.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose:true,
                fullscreen: false
            }).then(function(data){
              Api.Items.add(data, function(data){
                  if (data.success == true) updateItemsList();
              });
            });
        };

        $scope.card = {
            Data: [],
            currentPage: 1,
            pageSize: 10
        };

        function updateItemsList(){
          Api.Items.get(function(data){
              $scope.card.Data = data.response;
              $scope.loaded  = true;
          });
        }

        function getTranslates(){
            $translate([
                'Items.btn.addItem',
                'Items.btn.search',
                'Items.card.header.title',
                'Items.card.btn.view'
            ]).then(function (data) {
                $scope.translations = data;
            });
        }

    }
]);
