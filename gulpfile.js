var gulp            = require('gulp');
var minifyCss       = require('gulp-minify-css');
var autoprefixer    = require('gulp-autoprefixer');
var concat          = require('gulp-concat');
var uglify          = require('gulp-uglify');
var server          = require('gulp-express');
var jsonminify      = require('gulp-jsonminify');
var rename          = require('gulp-rename');


gulp.task('json', function(){
    gulp.src('src/locals/ru.json').pipe(jsonminify()).pipe(rename('admin-ru.json')).pipe(gulp.dest('build/locals/'));
    gulp.src('src/locals/en.json').pipe(jsonminify()).pipe(rename('admin-en.json')).pipe(gulp.dest('build/locals/'));
});

gulp.task('css', function() {
    gulp.src('node_modules/angular-material/angular-material.min.css')
        .pipe(autoprefixer({browsers: ['last 15 versions'], cascade: false}))
        .pipe(minifyCss())
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('build/css'));

    gulp.src('src/css/**/*.css')
        .pipe(autoprefixer({browsers: ['last 15 versions'], cascade: false}))
        .pipe(minifyCss())
        .pipe(concat('main-admin.css'))
        .pipe(gulp.dest('build/css'));
});

gulp.task('js', function() {
  gulp.src('node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js'),
  gulp.src('node_modules/ng-file-upload/dist/ng-file-upload.min.js')
  gulp.src('src/js/**/*.js')
        .pipe(concat('main-admin.js'))
        .pipe(gulp.dest('build/js'));
});

gulp.task('server', function(){
    server.run(['app.js']);
    gulp.watch(['app.js', 'app/**/*.js'], [server.run]);
});

gulp.task('watch', function () {
    gulp.watch('src/**/*.css', ['css']);
    gulp.watch('src/**/*.js', ['js']);
    gulp.watch('src/**/*.json', ['json']);
}); 

gulp.task('default', ['watch', 'server']);
