var express     = require('express');
var routes      = require('./app/routes');
var controllers = require('./app/controllers');
var config      = require('./app/libs/config');

require('events').EventEmitter.defaultMaxListeners = Infinity;

var app = express();

routes.setup(app, controllers);

app.use(express.static( __dirname + '/build' ));
app.listen(config.get('port'), config.get('ip'), function(){
    console.log('Server started on: ' + config.get('ip') +":"+config.get('port'));
});
//  "ip" : "127.0.0.1",
// "ip" : "10.2.0.5",